Guix extensions
===============

Install
-------

Clone the repository:
```
$ git clone https://gitlab.inria.fr/rgarbage/guix-extensions
```

Export the `GUIX_EXTENSIONS_PATH` environment variable:
```
$ export GUIX_EXTENSIONS_PATH=`pwd`/guix-extensions/guix/extensions
```

You can now use the extensions in the current shell.

To use the extensions permanently, add to your `.bashrc`/`.zshrc` the previous `export` line.
