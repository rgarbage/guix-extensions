;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2015-2022 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2019 Simon Tournier <zimon.toutoune@gmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix extensions list)
  #:use-module (guix ui)
  #:use-module (guix graph)
  #:use-module (guix scripts)
  #:use-module (guix packages)
  #:use-module (guix monads)
  #:use-module (guix store)
  #:use-module (guix gexp)
  #:use-module (guix derivations)
  #:use-module (guix memoization)
  #:use-module (guix modules)
  #:use-module ((guix build-system gnu) #:select (standard-packages))
  #:use-module (gnu packages)
  #:use-module (guix sets)
  #:use-module ((guix diagnostics)
                #:select (location-file formatted-message))
  #:use-module ((guix transformations)
                #:select (show-transformation-options-help
                          options->transformation
                          %transformation-options))
  #:use-module ((guix scripts build)
                #:select (%standard-build-options
                          %standard-native-build-options
                          show-native-build-options-help))
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:use-module (srfi srfi-37)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:export (%package-node-type
            %reverse-package-node-type
            %bag-node-type
            %bag-with-origins-node-type
            %bag-emerged-node-type
            %reverse-bag-node-type
            %derivation-node-type
            %reference-node-type
            %referrer-node-type
            %module-node-type
            %node-types

            guix-list))


;;;
;;; Package DAG.
;;;

(define (node-full-name thing)
  "Return a human-readable name to denote THING, a package, origin, or file
name."
  (cond ((package? thing)
         (package-full-name thing))
        ((origin? thing)
         (origin-actual-file-name thing))
        ((string? thing)                          ;file name
         (or (basename thing)
             (error "basename" thing)))
        (else
         (number->string (object-address thing) 16))))

(define (package-node-edges package)
  "Return the list of dependencies of PACKAGE."
  (match (package-direct-inputs package)
    (((labels packages . outputs) ...)
     ;; Filter out origins and other non-package dependencies.
     (filter package? packages))))

(define assert-package
  (match-lambda
    ((? package? package)
     package)
    (x
     (raise
      (formatted-message (G_ "~a: invalid argument (package name expected)")
                         x)))))

(define nodes-from-package
  ;; The default conversion method.
  (lift1 (compose list assert-package) %store-monad))

(define %package-node-type
  ;; Type for the traversal of package nodes.
  (node-type
   (name "package")
   (description "the DAG of packages, excluding implicit inputs")
   (convert nodes-from-package)

   ;; We use package addresses as unique identifiers.  This generally works
   ;; well, but for generated package objects, we could end up with two
   ;; packages that are not 'eq?', yet map to the same derivation (XXX).
   (identifier (lift1 object-address %store-monad))
   (label node-full-name)
   (edges (lift1 package-node-edges %store-monad))))


;;;
;;; Reverse package DAG.
;;;

(define (all-packages)            ;XXX: duplicated from (guix scripts refresh)
  "Return the list of all the distro's packages."
  (fold-packages (lambda (package result)
                   ;; Ignore deprecated packages.
                   (if (package-superseded package)
                       result
                       (cons package result)))
                 '()
                 #:select? (const #t)))           ;include hidden packages

(define %reverse-package-node-type
  ;; For this node type we first need to compute the list of packages and the
  ;; list of back-edges.  Since we want to do it only once, we use the
  ;; promises below.
  (let* ((packages   (delay (all-packages)))
         (back-edges (delay (run-with-store #f    ;store not actually needed
                              (node-back-edges %package-node-type
                                               (force packages))))))
    (node-type
     (inherit %package-node-type)
     (name "reverse-package")
     (description "the reverse DAG of packages")
     (edges (lift1 (force back-edges) %store-monad)))))

;;;
;;; List style export
;;;

(define (emit-list-prologue name port)
  (format port "" name))

(define (emit-list-epilogue port)
  (format port ""))

(define (emit-list-node id label port)
  (format port "~a~%" label))

(define (emit-list-edge id1 id2 port)
  (format port ""))

(define %list-backend
  (graph-backend "list"
                 "Generate list style graph."
                 emit-list-prologue emit-list-epilogue
                 emit-list-node emit-list-edge))


;;;
;;; Displaying a path.
;;;

(define (display-path node1 node2 type)
  "Display the shortest path from NODE1 to NODE2, of TYPE."
  (mlet %store-monad ((path (shortest-path node1 node2 type)))
    (define node-label
      (let ((label (node-type-label type)))
        ;; Special-case derivations and store items to print them in full,
        ;; contrary to what their 'node-type-label' normally does.
        (match-lambda
          ((? derivation? drv) (derivation-file-name drv))
          ((? string? str) str)
          (node (label node)))))

    (if path
        (format #t "~{~a~%~}" (map node-label path))
        (leave (G_ "no path from '~a' to '~a'~%")
               (node-label node1) (node-label node2)))
    (return #t)))


;;;
;;; Command-line options.
;;;

(define %options
  (cons* (option '(#\r "reverse") #f #f
                 (lambda (opt name arg result)
                   (alist-cons 'node-type %reverse-package-node-type
                               result)))
         (option '(#\M "max-depth") #t #f
                 (lambda (opt name arg result)
                   (alist-cons 'max-depth (string->number* arg)
                               result)))
         (option '(#\e "expression") #t #f
                 (lambda (opt name arg result)
                   (alist-cons 'expression arg result)))
         (find (lambda (option)
                (member "load-path" (option-names option)))
              %standard-build-options)
         (option '(#\h "help") #f #f
                 (lambda args
                   (leave-on-EPIPE (show-help))
                   (exit 0)))
         (option '(#\V "version") #f #f
                 (lambda args
                   (show-version-and-exit "guix graph")))

         (append %transformation-options
                 %standard-native-build-options)))

(define (show-help)
  ;; TRANSLATORS: Here 'dot' is the name of a program; it must not be
  ;; translated.
  (display (G_ "Usage: guix list PACKAGE...
List the dependencies of PACKAGE...\n"))
  (display (G_ "
  -r, --reverse          list the reverse dependencies of PACKAGE"))
  (display (G_ "
  -M, --max-depth=DEPTH  limit to nodes within distance DEPTH"))
  (display (G_ "
  -e, --expression=EXPR  consider the package EXPR evaluates to"))
  (newline)
  (display (G_ "
  -L, --load-path=DIR    prepend DIR to the package module search path"))
  (newline)
  (show-transformation-options-help)
  (newline)
  (display (G_ "
  -h, --help             display this help and exit"))
  (display (G_ "
  -V, --version          display version information and exit"))
  (newline)
  (show-native-build-options-help)
  (newline)
  (show-bug-report-information))

(define %default-options
  `((node-type . ,%package-node-type)
    (backend   . ,%list-backend)
    (max-depth . +inf.0)
    (system    . ,(%current-system))))


;;;
;;; Entry point.
;;;

(define-command (guix-list . args)
  (category packaging)
  (synopsis "view and query package dependency graphs")

  (define (shorter? str1 str2)
    (< (string-length str1) (string-length str2)))

  (define length-sorted
    (cut sort <> shorter?))

  (with-error-handling
    (define opts
      (parse-command-line args %options
                          (list %default-options)
                          #:build-options? #f))
    (define backend
      (assoc-ref opts 'backend))
    (define type
      (assoc-ref opts 'node-type))

    (with-store store
      (let* ((transform (options->transformation opts))
             (max-depth (assoc-ref opts 'max-depth))
             (items     (filter-map (match-lambda
                                      (('argument . (? store-path? item))
                                       item)
                                      (('argument . spec)
                                       (transform
                                        (specification->package spec)))
                                      (('expression . exp)
                                       (transform
                                        (read/eval-package-expression exp)))
                                      (_ #f))
                                    opts)))
        (when (null? items)
          (warning (G_ "no arguments specified; creating an empty graph~%")))

        (run-with-store store
          ;; XXX: Since grafting can trigger unsolicited builds, disable it.
          (mlet %store-monad ((_g    (set-grafting #f))
                              (nodes (mapm %store-monad
                                           (node-type-convert type)
                                           (reverse items))))
            (if (assoc-ref opts 'path?)
                ;; Sort by string length such that, in case of multiple
                ;; outputs, the shortest one (which corresponds to "out") is
                ;; picked (yup, a hack).
                (match nodes
                  (((= length-sorted (node1 _ ...))
                    (= length-sorted (node2 _ ...)))
                   (display-path node1 node2 type))
                  (_
                   (leave (G_ "'--path' option requires exactly two \
nodes (given ~a)~%")
                          (length nodes))))
                (export-graph (concatenate nodes)
                              (current-output-port)
                              #:node-type type
                              #:backend backend
                              #:max-depth max-depth)))
          #:system (assq-ref opts 'system)))))
  #t)

;;; graph.scm ends here
